import 'package:flutter/material.dart';

Color primary = const Color(0xFF687daf);

class Styles {
  static Color primaryColor = primary;
  static Color textColor = const Color(0xFF3b3b3b);
  static Color bgColor = const Color(0xFFeeedf2);
  static Color bgColorTable = const Color(0xffa3d984);
  static Color orangeColor = const Color(0xFF526799);
  static Color orangeColor2 = const Color(0xFFFFFFFF);
  static Color textM1 = const Color(0xffa79abf);
  static TextStyle textStyle =
      TextStyle(fontSize: 16, color: textColor, fontWeight: FontWeight.w500);
  static TextStyle headLineStyle1 =
      TextStyle(fontSize: 26, color: textColor, fontWeight: FontWeight.bold);
  static TextStyle headLineStyle2 =
      TextStyle(fontSize: 21, color: textColor, fontWeight: FontWeight.w500);
  static TextStyle headLineStyle3 = TextStyle(
      fontSize: 17, color: Colors.grey.shade500, fontWeight: FontWeight.w500);
  static TextStyle headLineStyle4 = TextStyle(
      fontSize: 14, color: Colors.grey.shade500, fontWeight: FontWeight.w500);
  static TextStyle headLineStyle5 = TextStyle(
      fontSize: 21, color: Colors.grey.shade500, fontWeight: FontWeight.w500);
  static TextStyle headLineStyle6 =
      TextStyle(fontSize: 21, color: orangeColor2, fontWeight: FontWeight.w500);
  static TextStyle textTable1 = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.bold,
  );
  static TextStyle textTable2 =
      TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: textM1);
  static TextStyle textTable3 = TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.white);
  static TextStyle textDay1 = TextStyle(color: textM1);
  static TextStyle headProflie1 =
      TextStyle(fontSize: 17, color: textColor, fontWeight: FontWeight.bold);
}
