import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:leran_flutter01/utils/app_layout.dart';

import '../utils/app_styles.dart';

class TableFinal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Styles.bgColor,
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                const Gap(25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Good Morning", style: Styles.headLineStyle3),
                        const Gap(5),
                        Text("Burapha University",
                            style: Styles.headLineStyle1),
                      ],
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                              fit: BoxFit.fitHeight,
                              image: AssetImage("assets/image/buuLogo.png"))),
                    ),
                  ],
                ),
                const Gap(25),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Color.fromARGB(210, 224, 196, 83),
            child: Column(
              children: [
                const Gap(10),
                Row(
                  children: [
                    Text(
                      "Exam   ",
                      style: Styles.textTable1,
                    ),
                    Text(
                      "Timetable",
                      style: Styles.textTable1,
                    ),
                  ],
                ),
                const Gap(15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("27"),
                        Text("มี.ค."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("28"),
                        Text("มี.ค."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("29"),
                        Text("มี.ค."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("30"),
                        Text("มี.ค."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("31"),
                        Text("มี.ค."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("1"),
                        Text("เม.ย."),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("2"),
                        Text("เม.ย."),
                        Text(""),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          cardClass1(),
          cardClass3(),
        ],
      ),
    );
  }
}

class cardClass1 extends StatelessWidget {
  const cardClass1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text("09:00"),
              LineGen(
                lines: [20.0, 30.0, 40.0, 10.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 223, 193, 26),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Color.fromARGB(210, 209, 212, 159),
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("09:00-12:00"),
                        VerticalDivider(),
                        Text(" 88624559-59"),
                        VerticalDivider(),
                        Text("IF-3M210"),
                      ],
                    ),
                  ),
                  Text(
                    "Software Testing",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class cardClass2 extends StatelessWidget {
  const cardClass2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text("13:00"),
              LineGen(
                lines: [20.0, 30.0, 40.0, 10.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 223, 193, 26),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Color.fromARGB(210, 209, 212, 159),
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("13:00 - 15:00"),
                        VerticalDivider(),
                        Text("88624459-59"),
                        VerticalDivider(),
                        Text("IF-3M210"),
                      ],
                    ),
                  ),
                  Text(
                    "Object-Oriented Analysis and Design",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class cardClass3 extends StatelessWidget {
  const cardClass3({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text("17:00"),
              LineGen(
                lines: [20.0, 30.0, 40.0, 10.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            decoration: BoxDecoration(
              color: Color.fromARGB(255, 223, 193, 26),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
              ),
            ),
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Color.fromARGB(210, 209, 212, 159),
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("17:00-20:00"),
                        VerticalDivider(),
                        Text("88624359-59"),
                        VerticalDivider(),
                        Text("IF-4C01"),
                      ],
                    ),
                  ),
                  Text(
                    "	Web Programming",
                    style:
                        TextStyle(fontSize: 21.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class LineGen extends StatelessWidget {
  final lines;
  const LineGen({
    Key? key,
    this.lines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        4,
        (index) => Container(
          height: 2.0,
          width: lines[index],
          color: Color(0xffd0d2d8),
          margin: EdgeInsets.symmetric(vertical: 14.0),
        ),
      ),
    );
  }
}
