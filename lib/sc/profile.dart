import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

import '../utils/app_styles.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Styles.bgColor,
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                const Gap(25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Good Morning", style: Styles.headLineStyle3),
                        const Gap(5),
                        Text("Profile Student", style: Styles.headLineStyle1),
                      ],
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                              fit: BoxFit.fitHeight,
                              image: AssetImage("assets/image/buuLogo.png"))),
                    )
                  ],
                )
              ],
            ),
          ),
          const Gap(30),
          Container(
            height: 280,
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Color.fromARGB(210, 209, 212, 159),
            child: Column(
              children: [
                const Gap(10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Name - Lastname", style: Styles.headLineStyle3),
                        const Gap(5),
                        Text("MR. JULLADIT KAEWRUNGRUANG",
                            style: Styles.headProflie1),
                        const Gap(5),
                        Text("Student ID", style: Styles.headLineStyle3),
                        Text("63160188", style: Styles.headProflie1),
                        const Gap(5),
                        Text("Faculty", style: Styles.headLineStyle3),
                        Text("Informatics", style: Styles.headProflie1),
                        const Gap(5),
                        Text("Major", style: Styles.headLineStyle3),
                        Text("Computer science", style: Styles.headProflie1),
                        const Gap(5),
                        Text("Campus", style: Styles.headLineStyle3),
                        Text("Bangsaen", style: Styles.headProflie1),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          height: 100,
                          width: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: const DecorationImage(
                                  fit: BoxFit.fitHeight,
                                  image:
                                      AssetImage("assets/image/person.png"))),
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
          const Gap(10),
          Container(
              height: 280,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              color: Color.fromARGB(210, 209, 212, 159),
              child: Column(
                children: [
                  const Gap(10),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("อ. ที่ปรึกษา:", style: Styles.headLineStyle3),
                            Text("อาจารย์ภูสิต กุลเกษม",
                                style: Styles.headProflie1),
                            const Gap(5),
                            Text("อ. ที่ปรึกษา:", style: Styles.headLineStyle3),
                            Text("ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน ",
                                style: Styles.headProflie1),
                            const Gap(5),
                            Text("ระดับการศึกษา:",
                                style: Styles.headLineStyle3),
                            Text("ปริญญาตรี  ", style: Styles.headProflie1),
                            const Gap(5),
                            Text("สถานภาพ:", style: Styles.headLineStyle3),
                            Text("กำลังศึกษา", style: Styles.headProflie1),
                            const Gap(5),
                            Text("คะแนนเฉลี่ยสะสม:",
                                style: Styles.headLineStyle3),
                            Text("ไม่ต้องรู้", style: Styles.headProflie1),
                          ],
                        ),
                      ])
                ],
              ))
        ],
      ),
    );
  }
}
